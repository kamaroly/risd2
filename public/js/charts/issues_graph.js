
$(function(){
  $.getJSON("/admin/api/dashboard/policing/typeofissues", function (result) {
    var labels = [],data=[];
    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].month);
        data.push(result[i].issues);
    }

    var issueTypeData = {
      labels : labels,
      datasets : [
        {
          fillColor : "#f0ec43",
          strokeColor : "#f0ec43",
          pointColor : "#f0ec43",
          pointStrokeColor : "#629751",
          data : data
        }
      ]
    };

    var issueTypes = document.getElementById('types_of_issues').getContext('2d');
    new Chart(issueTypes).Bar(issueTypeData,{});
  });
});