
$(function(){
  $.getJSON("/admin/api/dashboard/policing/typeofissues", function (result) {

    var labels = [],data=[];
    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].month);
        data.push(result[i].issues);
    }

    var genderOfVictimData = {
      labels : labels,
      datasets : [
        {
          fillColor : "#0e50a4",
          strokeColor : "#0e50a4",
          pointColor : "#0e50a4",
          pointStrokeColor : "#0e50a4",
          data : data
        }
      ]
    };

    var genderOfVictims = document.getElementById('gender_of_victim').getContext('2d');
    new Chart(genderOfVictims).Line(genderOfVictimData,{});
  });
});